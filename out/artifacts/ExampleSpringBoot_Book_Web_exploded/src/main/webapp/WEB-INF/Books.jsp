<%@page import="java.util.ArrayList" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BOOKS</title>
</head>
<body>
<div>
<table class="books" border="1" cellpadding="4" cellspacing="2" width="50%" align="center"
       style="overflow-wrap: anywhere; table-layout: fixed;background-color:burlywood">
    <tr>
        <th align="center" colspan="4"> LIST BOOKS</th>
    </tr>

    <tr>
        <th align="center"> ID</th>
        <th align="center"> NAME</th>
        <th align="center"> AUTHOR</th>
        <th align="center"> PRICE</th>
    </tr>
    <c:forEach var="book" items="${listBooks}"> </c:forEach>
    <tr>
        <td align="center"><a href="viewBook.do?id=${book.id}"> ${book.id}</a></td>
        <td align="center">${book.name}</td>
        <td align="center">${book.author}</td>
        <td align="center">${book.price}</td>
    </tr>
</table>

<input class="btn" type="button" value="ADD NEW BOOK" onclick="location.href='addNewBook.do'"/>

</div>
</body>
</html>