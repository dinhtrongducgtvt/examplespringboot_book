/**
 * Copyright(C) 2020 Luvina Software
 * BookNotFoundExeption.java , Nov 16, 2020 DucDT
 */
package com.techja.book.demo.error;

/**
 * @author dinhtrongduc
 *
 */
public class BookNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BookNotFoundException(Long id) {
		super("Book id not found : " + id);
	}

}
