package com.techja.book.demo.controller;

import com.techja.book.demo.entities.Book;
import com.techja.book.demo.repository.BookRepository;
import com.techja.book.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ListBookController {
    @Autowired
    BookService bookService;

    @RequestMapping("/")
    public String viewHomePage(Model model) {
        List<Book> listBooks = bookService.findAll();
//        for (Book b:listBooks
//             ) {
//            System.out.println(b.toString());
//        }
        model.addAttribute("listBooks",listBooks);
        return "Books";
    }

}
