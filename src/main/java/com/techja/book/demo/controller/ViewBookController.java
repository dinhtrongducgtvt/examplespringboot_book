package com.techja.book.demo.controller;

import com.techja.book.demo.entities.Book;
import com.techja.book.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewBookController {
    @Autowired
    BookService bookService;

    @RequestMapping(value = "viewBook", method = RequestMethod.GET)
    public String viewBook(Model model, @RequestParam(value = "id") String idStr) {
        int id = Integer.parseInt(idStr);
        System.out.println(id);
        Book book = bookService.findOne(id);
        model.addAttribute("book", book);
        return "ViewBook";
    }
}
