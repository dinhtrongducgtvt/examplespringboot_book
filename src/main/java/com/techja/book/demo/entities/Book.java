package com.techja.book.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table (name = "book")
public class Book {
    @Id
    @GeneratedValue
    @Column(name ="id")
    private int id;
    @Column(name ="name")
    private String name;
    @Column(name ="author")
    private String author;
    @Column(name ="price")
    private String price;


    /**
     *
     */

    public Book() {
        super();
    }

    /**
     * @param name
     * @param author
     * @param price
     */
    public Book(String name, String author, String price) {
        super();
        this.name = name;
        this.author = author;
        this.price = price;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

}
