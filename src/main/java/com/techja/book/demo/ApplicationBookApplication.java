package com.techja.book.demo;

import com.techja.book.demo.entities.Book;
import com.techja.book.demo.repository.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;

@SpringBootApplication
public class ApplicationBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationBookApplication.class, args);
    }
}
